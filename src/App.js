import React, { Fragment, useState } from 'react';

import {
  CssBaseline,
  Container,
} from '@material-ui/core';

import AppHeader from './components/AppHeader';
import CreationForm from './components/Forms'
import PaginatedPokeList from './components/Lists'

import {
  PokemonContext
} from './Context';

import './App.css';


const App = () => {

  const [pokemonList, setPokemonList] = useState([]);

  return (
    <Fragment>
      <CssBaseline />
      <AppHeader />
      <main>
        <PokemonContext.Provider value={{ pokemonList: pokemonList, setPokemonList: setPokemonList }}>
          <Container maxWidth="sm">
            <CreationForm />
          </Container>
          <Container>
            <PaginatedPokeList />
          </Container>
        </PokemonContext.Provider>
      </main>
    </Fragment>
  );
}

export default App;
