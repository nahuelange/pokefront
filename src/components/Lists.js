import React from 'react';

import {
    useState,
    useCallback,
    useEffect,
} from 'react';

import {
    Grid,
    LinearProgress,
    Paper,

} from '@material-ui/core';

import { makeStyles } from '@material-ui/core/styles';

import getPokemonPage from '../services/Pokedex';
import {
    PokemonContext
} from '../Context';


const useStyles = makeStyles(theme => ({
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: '#808080',
    },
}));

const PaginatedPokeList = () => {
    const classes = useStyles();

    const [loadStatus, setloadStatus] = useState(false);
    const [nextPage, setNextPage] = useState(false);
    const { pokemonList, setPokemonList } = React.useContext(PokemonContext);

    const loadPokemonPage = useCallback(async () => {
        if (nextPage != null) {
            const pokemons = await getPokemonPage(nextPage);

            setNextPage(pokemons.next);
            setPokemonList([...pokemonList, ...pokemons.results]);
        }
        setloadStatus(true);
    }, [pokemonList, nextPage]);


    useEffect(() => {
        loadPokemonPage();
    }, []);

    if (loadStatus) {

        return (
            <Grid container spacing={5} fullWidth={true}>
                {
                    pokemonList.map(
                        pokemon => (
                            <Grid key={pokemon.id} xs={4} item>
                                <Paper className={classes.paper} style={{ backgroundColor: pokemon.color_code }}>
                                    <h2>{pokemon.name}</h2>
                                </Paper>
                            </Grid>
                        )
                    )
                }
                {
                    (nextPage != null) ?
                        <Grid key="loadMore" xs={4} item onClick={loadPokemonPage}>
                            <Paper className={classes.paper}>
                                <h2>Load More</h2>
                            </Paper>
                        </Grid> :
                        <Grid xs={4} item>
                            <Paper className={classes.paper}>
                                <h2>No more pokemons</h2>
                            </Paper>
                        </Grid>
                }
            </Grid >
        )
    } else {
        return (
            <LinearProgress variant="query" />
        );
    }
};

export default PaginatedPokeList;
