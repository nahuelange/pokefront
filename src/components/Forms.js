import React from 'react';

import {
    useCallback,
    useState,
    useEffect,
} from 'react';

import {
    FormControl,
    InputLabel,
    Input,
    Select,
    FormHelperText,
    MenuItem,
    Button,
    Snackbar,
} from '@material-ui/core';

import Alert from '@material-ui/lab/Alert';

import {
    createPokemon,
    getColorList,
} from '../services/Pokedex';

import {
    PokemonContext
 } from '../Context';

 
const CreationForm = (props) => {
    const [pokename, setPokename] = useState('');
    const [pokecolor, setPokecolor] = useState("");
    const [colorList, setColorList] = useState([]);
    const [messages, setMessages] = useState([]);

    const { pokemonList, setPokemonList } = React.useContext(PokemonContext);

    const handleSubmit = useCallback(async (e) => {
        e.preventDefault();

        const {success, response} = await createPokemon({
            name: pokename,
            color: pokecolor,
        });

        if (success) {
            setPokecolor('');
            setPokename('');

            setMessages([{
                key: response.id,
                status: 'success',
                content: 'Pokemon successfully added to the Pokedex'
            }]);
            setPokemonList([response, ...pokemonList]);
        } else {
            const errors = [];
            for (const field in response) {
                errors.push({
                    key: field,
                    status: 'error',
                    field: field,
                    content: response[field],
                });
            }
            setMessages(errors);
        }
    }, [pokename, pokecolor, messages, pokemonList, setPokemonList]);

    const loadColorList = useCallback(async () => {
        setColorList(await getColorList());
    }, [colorList]);

    useEffect(() => {
        loadColorList();
    }, []);

    return (
        <form onSubmit={handleSubmit}>
            <FormControl required fullWidth={true}>
                <InputLabel htmlFor="my-input">Pokemon</InputLabel>
                <Input placeholder="Pokemon name" value={pokename} onChange={event => setPokename(event.target.value)} />
                <FormHelperText id="my-helper-text">The name of the pokemon.</FormHelperText>
            </FormControl>

            <FormControl required fullWidth={true}>
                <Select required value={pokecolor} onChange={event => setPokecolor(event.target.value)} displayEmpty>
                    <MenuItem value="" disabled>
                        <em>Select a Pokemon color</em>
                    </MenuItem>
                    {colorList.map(color => (
                        <MenuItem key={color.id} value={color.id}>{color.name}</MenuItem>
                    ))}
                </Select>
            </FormControl>
            <FormControl fullWidth={true}>
                <Button type="submit">Send to my Pokeball</Button>
            </FormControl>
            {messages.map(message => (
                <Snackbar key={message.key} open={true} autoHideDuration={10}>
                    <Alert severity={message.status}>
                        <strong>{message.field}</strong> {message.content}
                    </Alert>
                </Snackbar>
            ))}
        </form>
    );
};

export default CreationForm;
