import { API_URL } from '../Config';

const HTTP_201_CREATED = 201;

export const createPokemon = async (data) => {
    const response = await fetch(
        API_URL,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        });
    return {
        status: response.status === HTTP_201_CREATED,
        response: await response.json()
    };
};

export const getPokemonPage = async (page) => {
    const url = page ? page : API_URL;

    const result = await fetch(url);
    return result.json();
};

export const getColorList = async () => {
    const response = await fetch(
        `${API_URL}/color/`
    );
    return response.json();
};

export default getPokemonPage;
