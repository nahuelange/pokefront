# Pokefront

## Environment variables

Copy `env.dist` to `.env` and open it, then define the URL of the backend API as the variable `REACT_APP_API_URL`

## Deploy

Launch the below command and follow instructions:

```
npm run-script build
gcloud app deploy
```

That's it.
